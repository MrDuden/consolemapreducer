﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WordsCount
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Please enter file pass:");

            Dictionary<char, int> stringMap;
            IMapReducer<char, StreamReader> characterMapReducer = new CharacterMapReducer();
            char[] mapPattern = new char[] {'a','e','i','o','u'};
            string line;

            while ((line = Console.ReadLine()) != null)
            {
                var fileStream = GetFileStream(line);

                if (fileStream == null)
                {
                    Console.WriteLine("Enable to read file");
                    continue;
                }

                stringMap = characterMapReducer.Reduce(fileStream, mapPattern);
                PrintDictionary(stringMap);
            }
        }

        public static void PrintDictionary(Dictionary<char, int> dictionary)
        {
            foreach (var keyValuePair in dictionary)
            {
                Console.WriteLine(keyValuePair.Key + "  " + keyValuePair.Value);
            }
        }

        public static StreamReader GetFileStream(string filePath)
        {
            StreamReader streamReder;

            try
            {
                var path = Path.Combine(Directory.GetCurrentDirectory(), filePath);
                streamReder = new StreamReader(path);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }

            return streamReder;
        }
    }
}