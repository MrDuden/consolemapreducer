﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordsCount
{
    public interface IMapReducer<T, K>
    {
        Dictionary<T, int> Reduce(K source, T[] pattern);
    }
}
