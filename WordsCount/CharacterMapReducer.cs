﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordsCount
{
    public class CharacterMapReducer : IMapReducer<char, StreamReader>
    {
        Dictionary<char, int> IMapReducer<char, StreamReader>.Reduce(StreamReader text, char[] pattern)
        {
            var map = new Dictionary<char, int>();

            string line;

            while ((line = text.ReadLine()) != null)
            {
                foreach (var character in line)
                {
                    if (pattern.Contains(character))
                    {
                        if (map.ContainsKey(character))
                        {
                            map[character]++;
                        }
                        else
                        {
                            map.Add(character, 1);
                        }
                    }
                }
            }

            return map;
        }
    }
}
